interface A {
    fun a()
}

abstract class B {
    open fun a() {
        println("B")
    }
}

class C : A, B() {
    override fun a() {
        super.a()
    }
}

fun main(args: Array<String>) {
    C().a()
}
