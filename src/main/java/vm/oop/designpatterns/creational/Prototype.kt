package vm.oop.designpatterns.creational

/*
 * Prototype pattern
 *
 * Intent:
 * Allows cloning objects, even complex ones, without coupling to their specific classes. All prototype classes should
 * have a common interface that makes it possible to copy objects even if their concrete classes are unknown.
 * Avoid the inherent cost of creating a new object in the standard way (e.g., using the 'new' keyword) when it is
 * prohibitively expensive for a given application.
 * Create objects with type specified at run-time.
 * Instantiate dynamically loaded classes.
 *
 * Implementation:
 * Type of objects to create is determined by a prototypical instance, which is cloned to produce new objects.
 * Define a Prototype object that returns a copy of itself.
 * Create new objects by copying a Prototype object.
 * This enables configuration of a class with different Prototype objects.
 *
 * The Prototype pattern is available in Java out of the box with a Cloneable interface. Object.clone() can be called
 * only on Cloneable instances. Kotlin has the Cloneable interface too.
 *
 * Example:
 * Geometrical shape prototype registry.
 */

private object PrototypeExample1 {

    abstract class Shape : Cloneable {

        var x: Int
        var y: Int
        var color: String

        constructor() {
            x = 0
            y = 0
            color = "Black"
        }

        constructor(prototype: Shape) {
            x = prototype.x
            y = prototype.y
            color = prototype.color
        }

        public abstract override fun clone(): Shape

        override fun equals(other: Any?): Boolean {
            if (other !is Shape) return false
            return other.x == x && other.y == y && other.color == color
        }

        override fun hashCode(): Int {
            var result = x
            result = 31 * result + y
            result = 31 * result + color.hashCode()
            return result
        }
    }

    class Circle : Shape {

        var radius: Int

        constructor() : super() {
            radius = 0
        }

        constructor(prototype: Circle) : super(prototype) {
            radius = prototype.radius
        }

        override fun clone(): Shape {
            return Circle(this)
        }

        override fun equals(other: Any?): Boolean {
            if (other !is Circle || !super.equals(other)) return false
            return other.radius == radius
        }

        override fun hashCode(): Int {
            var result = super.hashCode()
            result = 31 * result + radius
            return result
        }

    }

    class Rectangle : Shape {

        var width: Int
        var height: Int

        constructor() : super() {
            width = 0
            height = 0
        }

        constructor(prototype: Rectangle) : super(prototype) {
            width = prototype.width
            height = prototype.height
        }

        override fun clone(): Rectangle {
            return Rectangle(this)
        }

        override fun equals(other: Any?): Boolean {
            if (other !is Rectangle || !super.equals(other)) return false
            return other.width == width && other.height == height
        }

        override fun hashCode(): Int {
            var result = super.hashCode()
            result = 31 * result + width
            result = 31 * result + height
            return result
        }
    }

    fun main() {
        // prototype registry
        val shapeRegistry = mapOf(
            "Big green circle" to Circle().apply {
                radius = 45
                color = "Green"
            },
            "Medium blue rectangle" to Rectangle().apply {
                width = 8
                height = 10
                color = "Blue"
            })

        fun getShape(key: String): Shape? {
            return shapeRegistry[key]?.clone()
        }

        val circle1 = getShape("Big green circle")
        val circle2 = getShape("Big green circle")
        val rectangle1  = getShape("Medium blue rectangle")

        // circle1 and circle2 are different objects but their fields are equal
        check(circle1 == circle2)
        check(circle1 is Circle)
        check(rectangle1 != circle1)
        check(rectangle1 is Rectangle)

        circle1.x = 10

        check(circle1 != circle2)
    }
}

fun main() {
    PrototypeExample1.main()
}
