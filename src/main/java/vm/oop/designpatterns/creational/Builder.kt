package vm.oop.designpatterns.creational

import java.awt.Button
import javax.swing.Icon

/*
 * Builder pattern
 *
 * Intent:
 * Provide a flexible solution to various object creation problems.
 * Separate the construction of a complex object from its representation.
 * Avoid modifying constructors after adding a new field to the class.
 *
 * Implementation:
 * Encapsulate creating and assembling the parts of a complex object in a separate Builder object.
 * A class delegates object creation to a Builder object instead of creating the objects directly.
 * A class can delegate to different Builder objects to create different representations of a complex object.
 * Allows making immutable objects because all properties can be set by the Builder with no need to use object setters.
 * Required parameters must be set in the Builder constructor.
 *
 * Example:
 * Dialog builder
 */

private object BuilderExample1 {

    class Dialog(
        private val title: String?,
        private val message: String,
        private val description: String?,
        private val icon: Icon?,
        private val button1: Button?,
        private val button2: Button?
    ) {

        class Builder(
            private val message: String
        ) {
            private var title: String? = null
            private var description: String? = null
            private var icon: Icon? = null
            private var button1: Button? = null
            private var button2: Button? = null

            fun title(title: String) = apply {
                this.title = title
            }

            fun description(description: String) = apply {
                this.description = description
            }

            fun icon(icon: Icon) = apply {
                this.icon = icon
            }

            fun button1(button: Button) = apply {
                this.button1 = button
            }

            fun button2(button: Button) = apply {
                this.button2 = button
            }

            fun build(): Dialog {
                return Dialog(title, message, description, icon, button1, button2)
            }
        }
    }

    fun main() {
        val dialog = Dialog.Builder("all is good")
            .title("Message")
            .build()
    }
}
