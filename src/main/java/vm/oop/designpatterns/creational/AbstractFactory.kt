package vm.oop.designpatterns.creational

/*
 * Abstract factory pattern
 *
 * Intent:
 * Provide a way to create families of related objects without imposing their concrete classes.
 * Allows for new derived types to be introduced with no change to the code that uses the base class.
 *
 * Implementation:
 * “Define an interface for creating families of related or dependent objects without specifying their concrete classes.”
 *
 * Abstract Factory is the factory of factories.
 *
 * Example:
 * GUI objects factories depending on platform.
 */

private object AbstractFactoryExample1 {

    // family of related GUI objects: Button, TextField

    interface Button {
        fun paint()
    }

    interface TextField {
        fun paint()
    }

    class WindowsButton : Button {
        override fun paint() {
            // render button in Windows
        }
    }

    class LinuxButton : Button {
        override fun paint() {
            // render button in Linux
        }
    }

    class WindowsTextField : TextField {
        override fun paint() {
            // render text field in Windows
        }
    }

    class LinuxTextField : TextField {
        override fun paint() {
            // render text field in Linux
        }
    }

    interface GuiAbstractFactory {
        fun createButton(): Button
        fun createTextField(): TextField
    }

    class WindowsGuiFactory : GuiAbstractFactory {
        override fun createButton(): Button {
            return WindowsButton()
        }

        override fun createTextField(): TextField {
            return WindowsTextField()
        }
    }

    class LinuxGuiFactory : GuiAbstractFactory {
        override fun createButton(): Button {
            return LinuxButton()
        }

        override fun createTextField(): TextField {
            return LinuxTextField()
        }
    }

    class App(platform: String) {

        val guiFactory: GuiAbstractFactory =
            // factory of factories
            when (platform) {
                "windows" -> WindowsGuiFactory()
                "linux" -> LinuxGuiFactory()
                else -> error("Unsupported platform")
            }
    }
}
