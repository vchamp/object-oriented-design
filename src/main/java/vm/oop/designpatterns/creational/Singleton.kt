package vm.oop.designpatterns.creational

/*
 * Singleton pattern
 *
 * Intent:
 * Restrict the instantiation of a class to a singular instance. Singletons are often preferred to global variables
 * because they do not pollute the global namespace. Additionally, they permit lazy allocation and initialization,
 * whereas global variables in many languages will always consume resources.
 *
 * Implementation:
 * Ensure that only one instance of the singleton class ever exists and provide global access to that instance.
 * Declare all constructors of the class to be private, which prevents it from being instantiated by other objects.
 * Provide a static method that returns a reference to the instance
 *
 * Cons:
 * Introduces global state, increases coupling, violates single responsibility principle.
 *
 * Example:
 * Logging
 */

private object SingletonExample1 {

    class Logger private constructor() {

        companion object {
            val instance: Logger by lazy {
                Logger()
            }
        }
    }

    class App {

        fun main() {
            val logger = Logger.instance
        }
    }
}
