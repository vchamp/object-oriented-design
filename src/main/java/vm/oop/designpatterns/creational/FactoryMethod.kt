package vm.oop.designpatterns.creational

/*
 * Factory method pattern
 *
 * Intent:
 * Create an object without specifying the exact class.
 *
 * Implementation:
 * "Define an interface for creating an object, but let subclasses decide which class to instantiate. The Factory method
 * lets a class defer instantiation it uses to subclasses."
 * Create objects by calling a factory method—either specified in an interface and implemented by child classes, or
 * implemented in a base class and optionally overridden by derived classes—rather than by calling a constructor.
 *
 * Example:
 * Create different configurations in different application types.
 */

private object FactoryMethodExample1 {

    interface Config {
        fun getDatabase(): String
    }

    interface App {
        fun factoryMethod(): Config {
            return DefaultConfig()
        }
    }

    class DebugConfig : Config {
        override fun getDatabase(): String {
            return "FileDatabase"
        }
    }

    class TestConfig : Config {
        override fun getDatabase(): String {
            return "MemoryDatabase"
        }
    }

    class DefaultConfig : Config {
        override fun getDatabase(): String {
            return "FileDatabase"
        }
    }

    class DebugApp : App {
        override fun factoryMethod(): Config {
            return DebugConfig()
        }
    }

    class TestApp : App {
        override fun factoryMethod(): Config {
            return TestConfig()
        }
    }
}
