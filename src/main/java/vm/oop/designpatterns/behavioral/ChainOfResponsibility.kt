package vm.oop.designpatterns.behavioral

import vm.oop.designpatterns.behavioral.ChainOfResponsibilityExample1.Logger
import vm.oop.designpatterns.behavioral.ChainOfResponsibilityExample1.Logger.Companion.consoleLogger
import vm.oop.designpatterns.behavioral.ChainOfResponsibilityExample1.Logger.Companion.emailLogger
import vm.oop.designpatterns.behavioral.ChainOfResponsibilityExample1.Logger.Companion.fileLogger

/*
 * Chain-of-responsibility pattern
 *
 * Intent:
 * Coupling the sender of a request to its receiver should be avoided.
 * It should be possible that more than one receiver can handle a request.
 * Implementing a request directly within the class that sends the request is inflexible because it couples the class
 * to a particular receiver and makes it impossible to support multiple receivers.
 *
 * Implementation:
 * Define a chain of receiver objects having the responsibility, depending on run-time conditions, to either handle
 * a request or forward it to the next receiver on the chain (if any).
 *
 * Example:
 * Logger chain
 */

private object ChainOfResponsibilityExample1 {

    fun interface Logger {

        enum class LogLevel {
            INFO, DEBUG, WARNING, ERROR, FUNCTIONAL_MESSAGE, FUNCTIONAL_ERROR;

            companion object {
                val all get() = values()
            }
        }

        companion object {

            fun writeLogger(levels: Array<out LogLevel>, consumer: (String) -> Unit) =
                Logger { msg, severity ->
                    if (levels.contains(severity)) {
                        consumer(msg)
                    }
                }

            fun consoleLogger(vararg levels: LogLevel) =
                writeLogger(levels) {
                    println("Writing to console: $it")
                }

            fun emailLogger(vararg levels: LogLevel) =
                writeLogger(levels) {
                    println("Sending via email: $it")
                }

            fun fileLogger(vararg levels: LogLevel) =
                writeLogger(levels) {
                    println("Writing to log file: $it")
                }
        }

        fun message(msg: String, severity: LogLevel)

        fun appendNext(nextLogger: Logger) =
            Logger { msg, severity ->
                message(msg, severity)
                nextLogger.message(msg, severity)
            }
    }

    fun main() {
        val logger = consoleLogger(*Logger.LogLevel.all)
            .appendNext(emailLogger(Logger.LogLevel.FUNCTIONAL_MESSAGE, Logger.LogLevel.FUNCTIONAL_ERROR))
            .appendNext(fileLogger(Logger.LogLevel.WARNING, Logger.LogLevel.ERROR))

        // handled by console logger
        logger.message("Entering function ProcessOrder().", Logger.LogLevel.DEBUG)
        logger.message("Order record retrieved.", Logger.LogLevel.INFO)

        // handled by console logger and email logger
        logger.message("Unable to Process Order ORD1 Dated D1 For Customer C1.", Logger.LogLevel.FUNCTIONAL_ERROR)
        logger.message("Order Dispatched.", Logger.LogLevel.FUNCTIONAL_MESSAGE)

        // handled by console logger and file logger
        logger.message("Customer Address details missing in Branch DataBase.", Logger.LogLevel.WARNING)
        logger.message("Customer Address details missing in Organization DataBase.", Logger.LogLevel.ERROR)
    }
}
