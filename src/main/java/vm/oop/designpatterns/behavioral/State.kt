package vm.oop.designpatterns.behavioral

/*
 * State
 *
 * Intent:
 * An object should change its behavior when its internal state changes.
 * State-specific behavior should be defined independently. That is, adding new states should not affect the behavior
 * of existing states.
 *
 * Implementation:
 * Define separate (state) objects that encapsulate state-specific behavior for each state. That is, define
 * an interface (state) for performing state-specific behavior, and define classes that implement the interface
 * for each state.
 * A class delegates state-specific behavior to its current state object instead of implementing state-specific
 * behavior directly.
 * This pattern is close to the concept of finite-state machines. The state pattern can be interpreted as a strategy
 * pattern, which is able to switch a strategy through invocations of methods defined in the pattern's interface.
 *
 * Example:
 * Mobile alert state.
 */

private object StateExample1 {

    interface MobileAlertState {

        fun alert(context: AlertStateContext)
    }

    class AlertStateContext {

        private var currentState: MobileAlertState = Vibration()

        fun setState(state: MobileAlertState) {
            currentState = state
        }

        fun alert() {
            currentState.alert(this)
        }
    }

    class Vibration : MobileAlertState {

        override fun alert(context: AlertStateContext) {
            println("vibration...")
        }
    }

    class Silent : MobileAlertState {

        override fun alert(context: AlertStateContext) {
            println("silent...")
        }
    }

    fun main() {
        val stateContext = AlertStateContext()
        stateContext.alert() // vibration...
        stateContext.alert() // vibration...
        stateContext.setState(Silent())
        stateContext.alert() // silent...
        stateContext.alert() // silent...
        stateContext.alert() // silent...
    }
}
