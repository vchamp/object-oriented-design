package vm.oop.designpatterns.behavioral

/*
 * Visitor pattern
 *
 * Intent:
 * Separate an algorithm from an object structure on which it operates.
 * It should be possible to define a new operation for (some) classes of an object structure without changing
 * the classes.
 *
 * Implementation:
 * Define a separate (visitor) object that implements an operation to be performed on elements of an object structure.
 * Clients traverse the object structure and call a dispatching operation accept (visitor) on an element — that
 * "dispatches" (delegates) the request to the "accepted visitor object". The visitor object then performs
 * the operation on the element ("visits the element").
 *
 * Example:
 * Json elements visitor.
 */

private object VisitorExample1 {

    interface JsonElement {
        val name: String?
        fun accept(visitor: JsonElementVisitor)
    }

    interface JsonElementVisitor {
        fun visit(item: JsonObject)
        fun visit(item: JsonValue)
        fun visit(item: JsonArray)
    }

    class JsonObject(override val name: String? = null) : JsonElement {

        lateinit var children: List<JsonElement>

        override fun accept(visitor: JsonElementVisitor) {
            visitor.visit(this)
        }
    }

    class JsonValue(override val name: String, val value: String) : JsonElement {

        override fun accept(visitor: JsonElementVisitor) {
            visitor.visit(this)
        }
    }

    class JsonArray(override val name: String?) : JsonElement {

        lateinit var children: List<JsonElement>

        override fun accept(visitor: JsonElementVisitor) {
            visitor.visit(this)
        }
    }

    class SearchJsonElementVisitor(private val searchString: String) : JsonElementVisitor {

        val searchResult = mutableListOf<JsonElement>()

        override fun visit(item: JsonObject) {
            for (child in item.children) {
                child.accept(this)
            }
        }

        override fun visit(item: JsonValue) {
            if (item.value.contains(searchString)) {
                searchResult += item
            }
        }

        override fun visit(item: JsonArray) {
            for (child in item.children) {
                child.accept(this)
            }
        }
    }

    class PrintJsonElementVisitor() : JsonElementVisitor {

        override fun visit(item: JsonObject) {
            println("Object: ${item.name ?: ""}")
        }

        override fun visit(item: JsonValue) {
            println("Value: ${item.name}=${item.value}")
        }

        override fun visit(item: JsonArray) {
            println("Array: ${item.name ?: ""}")
        }
    }

    fun main() {
        val document = JsonObject().apply {
            children = listOf(
                JsonValue("Title", "Machine Head"),
                JsonValue("Band", "Deep Purple"),
                JsonArray("Songs").apply {
                    children = listOf(
                        JsonValue("Title", "Highway Star"),
                        JsonValue("Title", "Maybe I'm a Leo")
                    )
                }
            )
        }
        document.accept(SearchJsonElementVisitor("star"))
        document.accept(PrintJsonElementVisitor())
    }
}
