package vm.oop.designpatterns.behavioral

/*
 * Iterator pattern
 *
 * Intent:
 * Decouples algorithms from containers.
 * The elements of an aggregate object should be accessed and traversed without exposing its representation (data structures).
 * New traversal operations should be defined for an aggregate object without changing its interface.
 *
 * Implementation:
 * Define a separate (iterator) object that encapsulates accessing and traversing an aggregate object.
 * Clients use an iterator to access and traverse an aggregate without knowing its representation (data structures).
 * New access and traversal operations can be defined independently by defining new iterators.
 *
 * Example:
 * Range iterator.
 */

private object IteratorExample1 {

    class Range(
        private val start: Int,
        private val end: Int
    ) : Iterable<Int> {

        override fun iterator() = object : Iterator<Int> {

            private var current = start

            override fun hasNext(): Boolean {
                return current < end
            }

            override fun next(): Int {
                if (!hasNext()) {
                    throw NoSuchElementException()
                }
                return current++
            }
        }
    }

    fun main() {
        val iterator = Range(0, 10).iterator()
        while (iterator.hasNext()) {
            println(iterator.next())
        }
        // or
        iterator.forEach { println(it) }
    }
}
