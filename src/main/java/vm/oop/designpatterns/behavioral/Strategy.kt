package vm.oop.designpatterns.behavioral

/*
 * Strategy pattern
 *
 * Intent:
 * Enables selecting an algorithm at runtime.
 *
 * Implementation:
 * Defines a family of algorithms and allows them to be used interchangeably. Encloses the differing logic in separate
 * classes while hiding it from clients behind the interface.
 * It is, in a sense, an extension of the Template Method pattern, but inversely to it, Strategy prefers composition
 * to inheritance.
 *
 * Example:
 * Billing strategy.
 */

private object StrategyExample1 {

    interface BillingStrategy {

        fun getActPrice(rawPrice: Double): Double
    }

    class NormalStrategy: BillingStrategy {

        override fun getActPrice(rawPrice: Double): Double {
            return rawPrice
        }
    }

    class HappyHourStrategy: BillingStrategy {

        override fun getActPrice(rawPrice: Double): Double {
            return rawPrice * 0.5
        }
    }

    class SumDependentStrategy: BillingStrategy {

        override fun getActPrice(rawPrice: Double): Double {
            return when {
                rawPrice > 40 -> rawPrice * 0.95
                rawPrice > 30 -> rawPrice * 0.85
                rawPrice > 20 -> rawPrice * 0.75
                else -> rawPrice
            }
        }
    }

    class CustomerBill(private val strategy: BillingStrategy) {

        private val drinks = mutableListOf<Double>()

        fun add(price: Double, quantity: Int) {
            drinks.add(strategy.getActPrice(price * quantity))
        }

        fun pay() {
            println("Total: ${drinks.sum()}")
            drinks.clear()
        }
    }

    fun main() {
        val normalStrategy = NormalStrategy()
        val happyHourStrategy = HappyHourStrategy()
        val sumDependentStrategy = SumDependentStrategy()

        val bill1 = CustomerBill(normalStrategy)
        bill1.add(20.0, 1)
        bill1.add(30.0, 2)
        val bill2 = CustomerBill(happyHourStrategy)
        bill2.add(15.0, 3)
        bill1.pay()
        bill2.pay()
        val bill3 = CustomerBill(sumDependentStrategy)
        bill3.add(10.0, 1)
        bill3.add(12.0, 2)
        bill3.pay()
    }
}
