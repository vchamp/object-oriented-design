package vm.oop.designpatterns.behavioral

import java.util.*

/*
 * Interpreter pattern
 *
 * Intent:
 * Specifies how to evaluate sentences in a language.
 * A grammar for a simple language should be defined so that sentences in the language can be interpreted.
 * When a problem occurs very often, it could be considered to represent it as a sentence in a simple language (Domain Specific Languages).
 *
 * Implementation:
 * The basic idea is to have a class for each symbol (terminal or nonterminal) in a specialized computer language.
 * The syntax tree of a sentence in the language is an instance of the composite pattern and is used to evaluate (interpret)
 * the sentence for a client.
 * Define a grammar for a simple language by defining an Expression class hierarchy and implementing an interpret() operation.
 * Represent a sentence in the language by an abstract syntax tree (AST) made up of Expression instances.
 * Interpret a sentence by calling interpret() on the AST.
 *
 * Example:
 * Process parsed statements.
 */

private object InterpreterExample1 {

    class Context {
        val result = Stack<String>()
    }

    interface Expression {
        fun interpret(context: Context)
    }

    abstract class OperatorExpression(
        private val left: Expression,
        private val right: Expression
    ) : Expression {

        override fun interpret(context: Context) {
            left.interpret(context)
            val leftValue = context.result.pop()

            right.interpret(context)
            val rightValue = context.result.pop()

            doInterpret(context, leftValue, rightValue)
        }

        abstract fun doInterpret(context: Context, leftValue: String, rightValue: String)
    }

    class EqualsExpression(left: Expression, right: Expression) : OperatorExpression(left, right) {
        override fun doInterpret(context: Context, leftValue: String, rightValue: String) {
            context.result.push(if (leftValue == rightValue) "true" else "false")
        }
    }

    class OrExpression(left: Expression, right: Expression) : OperatorExpression(left, right) {
        override fun doInterpret(context: Context, leftValue: String, rightValue: String) {
            context.result.push(if (leftValue == "true" || rightValue == "true") "true" else "false")
        }
    }

    class ValueExpression(private val value: String = "") : Expression {
        override fun interpret(context: Context) {
            context.result.push(value)
        }
    }

    fun main() {
        // parser is not included

        val context = Context()

        val input1 = ValueExpression("four")
        val expression1 = OrExpression(
            left = EqualsExpression(
                left = input1,
                right = ValueExpression("4")
            ),
            right = EqualsExpression(
                left = input1,
                right = ValueExpression("four")
            )
        )
        expression1.interpret(context)
        println(context.result.pop()) // output: true

        val input2 = ValueExpression("44")
        val expression2 = OrExpression(
            left = EqualsExpression(
                left = input2,
                right = ValueExpression("4")
            ),
            right = EqualsExpression(
                left = input2,
                right = ValueExpression("four")
            )
        )
        expression2.interpret(context)
        println(context.result.pop()) // output: false
    }
}
