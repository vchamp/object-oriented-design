package vm.oop.designpatterns.behavioral

import java.io.InputStream

/*
 * Observer
 *
 * Intent:
 * A one-to-many dependency between objects should be defined without making the objects tightly coupled.
 * When one object changes state, an open-ended number of dependent objects should be updated automatically.
 * An object can notify multiple other objects.
 *
 * Implementation:
 * Define Subject and Observer objects.
 * When a subject changes state, all registered observers are notified and updated automatically (and probably asynchronously).
 *
 * Example:
 * System input observers.
 */

private object ObserverExample1 {

    fun interface Observer {
        fun onEvent(event: String)
    }

    class Scanner(inputStream: InputStream) {

        private val reader = inputStream.bufferedReader()

        fun hasNext(): Boolean {
            return reader.ready()
        }

        fun nextLine(): String {
            return reader.readLine()
        }
    }

    class EventSource {

        private val observers = mutableListOf<Observer>()

        private fun notifyObservers(event: String) {
            observers.forEach { it.onEvent(event) }
        }

        fun addObserver(observer: Observer) {
            observers += observer
        }

        fun scanSystemIn() {
            val scanner = Scanner(System.`in`)
            while (scanner.hasNext()) {
                val line = scanner.nextLine()
                notifyObservers(line)
            }
        }
    }

    fun main() {
        println("Enter text:")
        val eventSource = EventSource()

        eventSource.addObserver { event ->
            println("Received response: $event")
        }

        eventSource.scanSystemIn()
    }
}

private object ObserverExample2 {

    interface Disposable {
        fun dispose()
    }

    interface Observable<T> {
        fun subscribe(observer: Observer<T>): Disposable
    }

    interface Observer<T> {
        fun onNext(item: T)
    }

    class Unsubscriber<T>(
        private val observer: Observer<T>,
        private val observers: MutableList<Observer<T>>
    ) : Disposable {

        override fun dispose() {
            if (observers.contains(observer)) {
                observers -= observer
            }
        }
    }

    data class Payload(val message: String)

    class Subject : Observable<Payload> {

        private val observers = mutableListOf<Observer<Payload>>()

        override fun subscribe(observer: Observer<Payload>): Disposable {
            if (!observers.contains(observer)) {
                observers += observer
            }
            return Unsubscriber(observer, observers)
        }

        fun sendMessage(message: String) {
            observers.forEach { it.onNext(Payload(message)) }
        }
    }

    fun main() {
        val subject = Subject()
        val disposables = Array<Disposable>(10) { index ->
            subject.subscribe(object: Observer<Payload> {
                override fun onNext(item: Payload) {
                    println("Observer ${index + 1} received ${item.message}")
                }
            })
        }
        subject.sendMessage("Message 1")
        subject.sendMessage("Message 2")
        disposables.forEach { it.dispose() }
    }
}
