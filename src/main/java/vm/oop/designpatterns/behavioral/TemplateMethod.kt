package vm.oop.designpatterns.behavioral

/*
 * Template method pattern
 *
 * Intent:
 * Define the skeleton of an algorithm in the superclass but let subclasses override specific steps of the algorithm
 * without changing its structure.
 * It is one of techniques to implement Inversion of control: the high-level code no longer determines what algorithms
 * to run; a lower-level algorithm is instead selected at run-time.
 *
 * Implementation:
 * The "template method" is implemented as a method in a base class (usually an abstract class). This method contains
 * code for the parts of the overall algorithm that are invariant. The template ensures that the overarching algorithm
 * is always followed. In the template method, portions of the algorithm that may vary are implemented by additional
 * helper methods.
 * Subclasses of the base class "fill in" the empty or "variant" parts of the "template" with specific algorithms that
 * vary from one subclass to another. It is important that subclasses do not override the template method itself.
 *
 * Example:
 * Database record save.
 */

private object TemplateMethodExample1 {

    abstract class DatabaseRecord() {

        // template method
        fun save() {
            println("Save $this")
            beforeSave()
            val isSuccess = DB.save(this)
            if (isSuccess) {
                afterSave()
            } else {
                failedSave()
            }
        }

        // hooks with default implementation

        open fun beforeSave() {
            // no-op
        }

        open fun afterSave() {
            // no-op
        }

        open fun failedSave() {
            // no-op
        }
    }

    object DB {

        fun save(record: DatabaseRecord): Boolean {
            println("DB is saving record: $record")
            return true // or false if failed
        }
    }

    class User(username: String) : DatabaseRecord() {

        var username: String = username
            private set

        override fun beforeSave() {
            println("Sanitizing User record")
            sanitizeRecord()
        }

        private fun sanitizeRecord() {
            username.trim()
            username = username.filter { it.isLetter() }
        }
    }

    class Post(val text: String) : DatabaseRecord()

    fun main() {
        val user1 = User("Alice")
        user1.save()
        val user2 = User("Bob")
        user2.save()
        val post = Post("Message")
        post.save()
    }
}
