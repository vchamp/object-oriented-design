package vm.oop.designpatterns.behavioral

import kotlin.properties.Delegates

/*
 * Mediator pattern
 *
 * Intent:
 * Defines an object that encapsulates how a set of objects interact.
 * Tight coupling between a set of interacting objects should be avoided.
 * It should be possible to change the interaction between a set of objects independently.
 *
 * Implementation:
 * Define a separate (mediator) object that encapsulates the interaction between a set of objects.
 * Objects delegate their interaction to a mediator object instead of interacting with each other directly.
 *
 * A common use of this pattern is ViewModel.
 *
 * Example:
 * Dialog as UI elements director.
 */

private object MediatorExample1 {

    // view manager, controlling UI elements inside it
    interface UiDirector {
        fun notify(sender: UiElement, event: String)
    }

    // `UiElement` class is a UI component inside `UiDirector`
    abstract class UiElement(val uiDirector: UiDirector)

    class Button(uiDirector: UiDirector) : UiElement(uiDirector) {
        fun click() {
            uiDirector.notify(this, "click")
        }
    }

    class TextBox(uiDirector: UiDirector) : UiElement(uiDirector) {
        val text: String by Delegates.observable("") { property, oldValue, newValue ->
            uiDirector.notify(this, "text_changed")
        }
    }

    // Dialog window, handles view state according to UI components events
    class DialogMediator : UiDirector {

        // dialog creates instances of its `components`
        private val okButton = Button(this)
        private val cancelButton = Button(this)
        private val input = TextBox(this)

        private var inputText = ""

        override fun notify(sender: UiElement, event: String) {
            when (sender) {
                // after each UI component change it gets update
                input -> if (event == "text_change") inputText = input.text
                // and has logic called after buttons are clicked
                okButton -> if (event == "click") submit()
                cancelButton -> if (event == "click") dismiss()
            }
        }
        private fun dismiss() {}
        private fun submit() {}
    }
}
