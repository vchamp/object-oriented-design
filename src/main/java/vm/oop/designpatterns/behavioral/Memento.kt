package vm.oop.designpatterns.behavioral

/*
 * Memento pattern
 *
 * Intent:
 * Exposes the private internal state of an object. One example of how this can be used is to restore an object to its
 * previous state (undo via rollback), another is versioning, another is custom serialization.
 * The internal state of an object should be saved externally so that the object can be restored to this state later.
 * The object's encapsulation must not be violated.
 *
 * Implementation:
 * Make an object (originator) itself responsible for saving its internal state to a (memento) object and restoring to
 * a previous state from a (memento) object.
 * Only the originator that created a memento is allowed to access it.
 * A client (caretaker) can request a memento from the originator (to save the internal state of the originator) and
 * pass a memento back to the originator (to restore to a previous state).
 *
 * Example:
 * Restore saved state.
 */

private object MementoExample1 {

    class Originator {

        private var state: String = ""

        fun set(state: String) {
            this.state = state
        }

        fun saveToMemento(): Memento {
            return Memento(state)
        }

        fun restoreFromMemento(memento: Memento) {
            state = memento.getSavedState()
        }

        class Memento(private val state: String) {

            // accessible by Originator only
            internal fun getSavedState(): String {
                return state
            }
        }
    }

    class Caretaker {

        fun main() {
            val savedStates = mutableListOf<Originator.Memento>()

            val originator = Originator()
            originator.set("State1")
            originator.set("State2")
            savedStates.add(originator.saveToMemento())
            originator.set("State3")
            // we can request multiple mementos, and choose which one to roll back to
            savedStates.add(originator.saveToMemento())
            originator.set("State4")

            originator.restoreFromMemento(savedStates[1])
        }
    }
}
