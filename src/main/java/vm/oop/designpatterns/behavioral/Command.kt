package vm.oop.designpatterns.behavioral

/*
 * Command pattern
 *
 * Intent:
 * Coupling the invoker of a request to a particular request should be avoided. That is, hard-wired requests should be avoided.
 * It should be possible to configure an object (that invokes a request) with a request.
 *
 * Implementation:
 * Define separate (command) objects that encapsulate a request.
 * A class delegates a request to a command object instead of implementing a particular request directly.
 * The class is no longer coupled to a particular request and has no knowledge (is independent) of how the request is carried out.
 * An object is used to encapsulate all information needed to perform an action or trigger an event at a later time.
 * Four terms always associated with the command pattern are command, receiver, invoker and client.
 *
 * Example:
 * Undoable shapes drawing.
 */

private object CommandExample1 {

    abstract class DrawCommand(private val canvas: Canvas) {

        private lateinit var preCommandState: List<Shape>

        abstract fun execute()

        fun saveState() {
            preCommandState = canvas.shapes.toList()
        }

        fun undo() {
            println("undo $this")
            canvas.shapes = preCommandState.toMutableList()
        }
    }

    interface Shape
    data class Line(val length: Int) : Shape
    data class Circle(val diameter: Int) : Shape

    // commands for drawing shapes, taking params and Receiver (Canvas)
    class DrawLine(private val length: Int, private val canvas: Canvas) : DrawCommand(canvas) {
        override fun execute() {
            saveState()
            canvas.draw(Line(length))
        }
    }

    class DrawCircle(private val diameter: Int, private val canvas: Canvas) : DrawCommand(canvas) {
        override fun execute() {
            saveState()
            canvas.draw(Circle(diameter))
        }
    }

    // Receiver
    class Canvas {

        var shapes = mutableListOf<Shape>()

        fun draw(shape: Shape) {
            println("drawing $shape")
            shapes.add(shape)
        }
    }

    // Client and Invoker
    fun main() {
        val canvas = Canvas()
        val commandsHistory = mutableListOf<DrawCommand>()

        val drawLine = DrawLine(2, canvas)
        drawLine.execute()
        commandsHistory.add(drawLine)

        val drawCircle = DrawCircle(1, canvas)
        drawCircle.execute()
        commandsHistory.add(drawCircle)

        val drawLongLine = DrawLine(10, canvas)
        drawLongLine.execute()
        commandsHistory.add(drawLongLine)

        val drawBigCircle = DrawCircle(12, canvas)
        drawBigCircle.execute()
        commandsHistory.add(drawBigCircle)

        println("current shapes: ${canvas.shapes}")
        println("--- undo last 2 ---")
        // reverting last 2 commands
        commandsHistory.removeLast().undo()
        commandsHistory.removeLast().undo()
        println("current shapes: ${canvas.shapes}")
    }
}

private object CommandExample2 {

    interface Command {
        fun execute()
    }

    // Invoker
    class Switch(
        private val closeCommand: Command,
        private val openCommand: Command
    ) {
        fun close() {
            closeCommand.execute()
        }

        fun open() {
            openCommand.execute()
        }
    }

    // An interface that defines actions that the receiver can perform
    interface Switchable {
        fun powerOn()
        fun powerOff()
    }

    // Receiver
    class Light : Switchable {

        override fun powerOn() {
            println("The light is on")
        }

        override fun powerOff() {
            println("The light is off")
        }
    }

    class CloseSwitchCommand(private val switchable: Switchable) : Command {
        override fun execute() {
            switchable.powerOff()
        }
    }

    class OpenSwitchCommand(private val switchable: Switchable) : Command {
        override fun execute() {
            switchable.powerOn()
        }
    }

    // Client
    fun main() {
        val lamp: Switchable = Light()

        val switchClose: Command = CloseSwitchCommand(lamp)
        val switchOpen: Command = OpenSwitchCommand(lamp)

        val switch = Switch(switchClose, switchOpen)
        switch.open()
        switch.close()
    }
}
