package vm.oop.designpatterns.structural

/*
 * Bridge pattern
 *
 * Intent:
 * Decouple an abstraction from its implementation so that the two can vary independently.
 * An abstraction and its implementation should be defined and extended independently of each other.
 * A compile-time binding between an abstraction and its implementation should be avoided so that an implementation can
 * be selected at run-time.
 *
 * Implementation:
 * Separate an abstraction (Abstraction) from its implementation (Implementer) by putting them in separate class hierarchies.
 * Implement the Abstraction in terms of (by delegating to) an Implementer object.
 *
 * Example:
 * Bank account that separates the account operations from the logging of these operations.
 */

private object BridgeExample1 {

    // implementer
    interface Logger {

        companion object {

            fun info() = object : Logger {
                override fun log(message: String) {
                    println("info: $message")
                }
            }

            fun warning() = object : Logger {
                override fun log(message: String) {
                    println("warning: $message")
                }
            }
        }

        fun log(message: String)
    }

    // abstraction
    abstract class AbstractAccount{

        private var logger = Logger.info()

        fun setLogger(logger: Logger) {
            this.logger = logger
        }

        // the logging part is delegated to the Logger implementation
        protected fun operate(message: String, result: Boolean) {
            logger.log("$message result $result")
        }
    }

    class SimpleAccount(
        private var balance: Int
    ): AbstractAccount() {

        fun isBalanceLow() =
            balance < 50

        fun withdraw(amount: Int) {
            val shouldPerform = balance >= amount
            if (shouldPerform) {
                balance -= amount
            }
            operate("withdraw $amount", shouldPerform)
        }
    }

    fun main() {
        val account = SimpleAccount(100)
        account.withdraw(75)

        if (account.isBalanceLow()) {
            // we can also change the Logger implementation at runtime
            account.setLogger(Logger.warning())
        }

        account.withdraw(10)
        account.withdraw(100)
    }
}
