package vm.oop.designpatterns.structural

/*
 * Facade pattern
 *
 * Intent:
 * Facade is an object that serves as a front-facing interface masking more complex underlying or structural code.
 * Improve the readability and usability of a software library by masking interaction with more complex components
 * behind a single (and often simplified) API.
 * Provide a context-specific interface to more generic functionality (complete with context-specific input validation).
 * Serve as a launching point for a broader refactor of monolithic or tightly-coupled systems in favor of more
 * loosely-coupled code.
 *
 * Implementation:
 * Implements a simple interface in terms of (by delegating to) the interfaces in the subsystem and may perform
 * additional functionality before/after forwarding a request.
 *
 * Example:
 * Repositories
 */

private object FacadeExample1 {

    data class User(val id: String, val name: String)

    // facade interface
    interface UserRepository {
        fun getUser(id: String): User
    }

    // UserRepository, UserDb, UserApi and UserCache may have different implementations,
    // but it does not matter for the client
    class DefaultUserRepository(
        private val userDb: UserDb,
        private val userApi: UserApi,
        private val userCache: UserCache
    ) : UserRepository {

        override fun getUser(id: String): User {
            // check if the object is in the cache
            val cashedUser = userCache.get(id)
            if (cashedUser == null) {
                // if not, check database
                val dbUser = userDb.get(id)
                return if (dbUser == null) {
                    // if not, check on remote server
                    val userDto = userApi.get(id)
                    userDto.toUser().also {
                        // after getting the object, put it in DB and cache
                        userDb.add(it.toEntity())
                        userCache.add(it)
                    }
                } else {
                    // if object is in DB, return it and put it in cache
                    dbUser.toUser().also {
                        userCache.add(it)
                    }
                }
            } else {
                return cashedUser
            }
        }

        interface UserDb {
            fun get(id: String): UserEntity?
            fun add(user: UserEntity)
        }

        interface UserApi {
            fun get(id: String): UserDto
        }

        interface UserCache {
            fun get(id: String): User?
            fun add(user: User)
        }

        data class UserDto(val id: String, val name: String)

        data class UserEntity(val id: String, val name: String)

        // useful extension functions, mapping entity and DTO to domain class
        private fun UserDto.toUser(): User {
            return User(this.id, this.name)
        }

        private fun UserEntity.toUser(): User {
            return User(this.id, this.name)
        }

        private fun User.toEntity(): UserEntity {
            return UserEntity(this.id, this.name)
        }
    }

    // UserRepository instance could be injected
    fun main(repository: UserRepository) {
        // Repository is a Facade for overall data access. Client doesn't have to bother with cache, API call, DB etc.
        repository.getUser("1")
    }
}
