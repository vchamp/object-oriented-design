package vm.oop.designpatterns.structural

/*
 * Adapter pattern
 *
 * Intent:
 * Allows the interface of an existing class to be used as another interface.It is often used to make existing classes
 * work with others without modifying their source code.
 *
 * Implementation:
 * Define a separate adapter class that converts the (incompatible) interface of a class (adaptee) into another
 * interface (target) clients require.
 * Work through an adapter to work with (reuse) classes that do not have the required interface.
 *
 * Example:
 * Manipulate text view as shape.
 */

private object AdapterExample1 {

    interface Shape {
        fun draw()
        fun createManipulator(): ShapeManipulator<out Shape>
    }

    interface ShapeManipulator<T : Shape> {
        fun drag()
        fun resize(scale: Float)
    }

    class Circle : Shape {

        override fun draw() {}

        override fun createManipulator() = object : ShapeManipulator<Circle> {
            override fun drag() = println("Manipulating circle ${this@Circle}")
            override fun resize(scale: Float) = println("Resizing circle ${this@Circle}")
        }
    }

    class TextView {
        fun displayText() {}
        fun changeSize() {}
        fun changePosition() {}
    }

    class TextViewAdapter(val textView: TextView) : Shape {
        override fun draw() {
            textView.displayText()
        }

        override fun createManipulator() = object : ShapeManipulator<Shape> {
            override fun drag() {
                textView.changePosition()
            }

            override fun resize(scale: Float) {
                textView.changeSize()
            }
        }
    }

    class Window() {
        fun drawShape(shape: Shape) {
        }
    }

    fun main() {
        val window = Window()
        val circle = Circle()
        window.drawShape(circle)

        val textView = TextView()
        // window.drawShape(textView) // error! wrong interface
        window.drawShape(TextViewAdapter(textView)) // using Adapter

        TextViewAdapter(textView).createManipulator().resize(0.5f)
    }
}
