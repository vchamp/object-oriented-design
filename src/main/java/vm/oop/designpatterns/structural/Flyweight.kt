package vm.oop.designpatterns.structural

/*
 * Flyweight pattern
 *
 * Intent:
 * Use sharing to support large numbers of fine-grained objects efficiently.
 * Object that minimizes memory usage by sharing some of its data with other similar objects.
 * Useful when dealing with large numbers of objects with simple repeated elements that would use a large amount of
 * memory if individually stored.
 *
 * Implementation:
 * Store intrinsic state that is invariant, context-independent and shareable.
 * Provide an interface for passing in an extrinsic state that is variant, context-dependent and can't be shared.
 *
 * Example:
 * Data structures representing characters in a word processor.
 */

private object FlyweightExample1 {

    class FontMetrics(val fontName: String) {
        companion object {
            fun get(fontName: String): FontMetrics {
                // long initialization
                return FontMetrics(fontName)
            }
        }
        // big data
    }

    // Flyweight with intrinsic state
    data class Glyph(
        val fontMetrics: FontMetrics,
        val formattingData: Any
    ) {
        // passing in extrinsic state
        fun render(x: Int, y: Int) {
            // render glyph
        }
    }

    data class Character(val name: Char)

    object GlyphFactory {

        private val glyphs = mutableMapOf<Char, Glyph>()

        fun getGlyph(char: Char): Glyph {
            var glyph = glyphs[char]
            if (glyph == null) {
                glyph = Glyph(FontMetrics.get("sans-serif"), ByteArray(10_000))
                glyphs[char] = glyph
            }
            return glyph
        }
    }

    class Document {

        private val characters = mutableListOf<Character>()

        fun addCharacter(name: Char) {
            characters.add(Character(name))
        }

        fun print() {
            for ((index, character) in characters.withIndex()) {
                print(character, index)
            }
        }

        fun print(character: Character, position: Int) {
            val glyph = GlyphFactory.getGlyph(character.name)
            val x = position % 80
            val y = position / 80
            glyph.render(x, y)
        }
    }

    fun main() {
        val document = Document()
        // many characters sharing Glyph flyweights
        for (i in 0..1000) {
            document.addCharacter('a')
            document.print()
            document.addCharacter('b')
            document.print()
            document.addCharacter('c')
            document.print()
        }
    }
}
