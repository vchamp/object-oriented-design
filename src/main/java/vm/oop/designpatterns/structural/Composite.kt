package vm.oop.designpatterns.structural

/*
 * Composite pattern
 *
 * Intent:
 * Compose objects into tree structures to represent part-whole hierarchies. Implementing the composite pattern lets
 * clients treat individual objects and compositions uniformly.
 * If we define Part objects and Whole objects that act as containers for Part objects, then clients must treat them
 * separately, which complicates client code.
 *
 * Implementation:
 * Define a unified Component interface for both part (Leaf) objects and whole (Composite) objects.
 * Individual Leaf objects implement the Component interface directly, and Composite objects forward requests to their
 * child components.
 *
 * Example:
 * Graphic class, which can be either an ellipse or a composition of several graphics.
 */

private object CompositeExample1{

    interface Graphic {
        fun print()
    }

    class CompositeGraphic : Graphic {

        private val childGraphics = mutableListOf<Graphic>()

        fun add(graphic: Graphic) {
            childGraphics.add(graphic)
        }

        override fun print() {
            for (graphic in childGraphics) {
                graphic.print()
            }
        }
    }

    class Ellipse : Graphic {
        override fun print() {
            println("Ellipse")
        }
    }

    fun main() {
        val ellipse1 = Ellipse()
        val ellipse2 = Ellipse()
        val ellipse3 = Ellipse()
        val ellipse4 = Ellipse()

        val compositeGraphic1 = CompositeGraphic()
        compositeGraphic1.apply {
            add(ellipse1)
            add(ellipse2)
            add(ellipse3)
        }

        val compositeGraphic2 = CompositeGraphic()
        compositeGraphic2.apply {
            add(ellipse4)
        }

        val compositeGraphic = CompositeGraphic()
        compositeGraphic.apply {
            add(compositeGraphic1)
            add(compositeGraphic2)
        }

        compositeGraphic.print()
    }
}
