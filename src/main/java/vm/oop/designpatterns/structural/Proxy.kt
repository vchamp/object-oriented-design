package vm.oop.designpatterns.structural

/*
 * Proxy pattern
 *
 * Intent:
 * Class functioning as an interface to some object, to which the access should be controlled.
 * The proxy could interface to anything: a network connection, a large object in memory, a file, or some other
 * resource that is expensive or impossible to duplicate. In the proxy, extra functionality can be provided,
 * for example caching when operations on the real object are resource intensive, or checking preconditions before
 * operations on the real object are invoked.
 *
 * Implementation:
 * Define a separate Proxy object that can be used as a substitute for another object (Subject) and implements
 * additional functionality to control the access to this subject.
 *
 * Example:
 * Cached image.
 */

private object ProxyExample1 {

    interface Image {
        fun displayImage()
    }

    class RealImage(private val fileName: String) : Image {

        init {
            loadImageFromDisk()
        }

        private fun loadImageFromDisk() {
            println("Loading $fileName")
        }

        override fun displayImage() {
            println("Displaying $fileName")
        }
    }

    class ProxyImage(private val fileName: String) : Image {

        private var image: RealImage? = null

        override fun displayImage() {
            val image = image ?: RealImage(fileName).also { image = it }
            image.displayImage()
        }
    }

    fun main() {
        val image1 = ProxyImage("HiRes_10MB_Photo1")
        val image2 = ProxyImage("HiRes_10MB_Photo2")

        image1.displayImage() // loading from disk
        image1.displayImage() // using cached
        image2.displayImage() // loading from disk
        image2.displayImage() // using cached
        image1.displayImage() // using cached
    }
}
