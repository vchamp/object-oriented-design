package vm.oop.designpatterns.structural

/*
 * Decorator pattern
 *
 * Intent:
 * Add behavior to an individual object, dynamically, without affecting the behavior of other objects from the same class.
 * Responsibilities should be added to (and removed from) an object dynamically at run-time.
 * A flexible alternative to subclassing for extending functionality should be provided.
 *
 * Implementation:
 * Implement the interface of the extended (decorated) object (Component) transparently by forwarding all requests to it.
 * Perform additional functionality before/after forwarding a request.
 * This pattern is designed so that multiple decorators can be stacked on top of each other, each time adding a new
 * functionality to the overridden method(s).
 *
 * Example:
 * Vertical and horizontal scrollbars in a Window.
 */

private object DecoratorExample1 {

    interface Window {
        fun draw()
        fun getDescription(): String
    }

    class SimpleWindow : Window {

        override fun draw() {
            // draw window
        }

        override fun getDescription(): String {
            return "simple window"
        }
    }

    abstract class WindowDecorator(private val windowToBeDecorated: Window) : Window {

        override fun draw() {
            windowToBeDecorated.draw() // delegation
        }

        override fun getDescription(): String {
            return windowToBeDecorated.getDescription() // delegation
        }
    }

    class VerticalScrollBarDecorator(windowToBeDecorated: Window) : WindowDecorator(windowToBeDecorated) {

        override fun draw() {
            super.draw()
            drawVerticalScrollBar()
        }

        private fun drawVerticalScrollBar() {
            // draw the vertical scrollbar
        }

        override fun getDescription(): String {
            return super.getDescription() + ", including vertical scrollbar"
        }
    }

    class HorizontalScrollBarDecorator(windowToBeDecorated: Window) : WindowDecorator(windowToBeDecorated) {

        override fun draw() {
            super.draw()
            drawHorizontalScrollBar()
        }

        private fun drawHorizontalScrollBar() {
            // draw the horizontal scrollbar
        }

        override fun getDescription(): String {
            return super.getDescription() + ", including horizontal scrollbar"
        }
    }

    fun main() {
        val decoratedWindow =
            HorizontalScrollBarDecorator(
                VerticalScrollBarDecorator(
                    SimpleWindow()
                )
            )
        println(decoratedWindow.getDescription())
    }
}

private object DecoratorWithDelegatesExample {

    // basic interface to be decorated
    interface Component {
        fun sayHello(): String
    }

    class BasicComponent : Component {
        override fun sayHello() = "hello from ${javaClass.simpleName}"
    }

    // delegating the `Component` interface behavior to the `component` instance passed in constructor
    abstract class Decorator(protected val component: Component) : Component by component

    class Decorator1(component: Component) : Decorator(component) {
        // component method overridden by decorator, extending the message
        override fun sayHello() = "${component.sayHello()} and from ${javaClass.simpleName}"
    }

    // no need to override `sayHello()` every time
    // it was "delegated" to the `component` instance from constructor
    class Decorator2(component: Component) : Decorator(component)

    fun main() {
        val basic: Component = BasicComponent()
        val decOne: Component = Decorator1(basic)
        val decTwo: Component = Decorator2(decOne)
        println(basic.sayHello())  // hello from BasicComponent
        println(decOne.sayHello()) // hello from BasicComponent and from Decorator1
        println(decTwo.sayHello()) // hello from BasicComponent and from Decorator1 and from Decorator2
    }
}
