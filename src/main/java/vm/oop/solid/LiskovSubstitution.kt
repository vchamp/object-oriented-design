package vm.oop.solid

import java.io.File

/*
 * Liskov substitution principle:
 * Objects should be replaceable with their subtypes without altering the correctness of a program.
 * The inheriting class should have a use case for all the properties and behavior of the inherited class.
 * If a class inherits another, it should do so in a manner that all the properties of the inherited class would remain
 * relevant to its functionality.
 */

private object LiskovSubstitutionViolated {

    open class FileLogger {

        open fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }

    // FileLogger is not replaceable by CustomFileLogger, users will not use customLogError() although it was intended.
    // logError() should be relevant to CustomFileLogger's functionality.
    class CustomFileLogger : FileLogger() {

        fun customLogError(error: String) {
            val file = File("custom_error_file.txt")
            file.appendText(error)
        }
    }
}

private object LiskovSubstitutionFixed {

    open class FileLogger {

        open fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }

    // logError() is now relevant to CustomFileLogger's functionality
    class CustomFileLogger : FileLogger() {

        override fun logError(error: String) {
            val file = File("custom_error_file.txt")
            file.appendText(error)
        }
    }
}

private object Example1Violated {

    open class Rectangle {

        private var width: Int = 0
        private var height: Int = 0

        fun setWidth(w: Int) {
            width = w
        }

        fun setHeight(h: Int) {
            height = h
        }
    }

    // Rectangle is not replaceable with Square because Square cannot have different width and height
    class Square : Rectangle()
}
