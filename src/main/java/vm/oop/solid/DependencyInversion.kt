package vm.oop.solid

import java.io.File

/*
 * Dependency inversion principle
 * Depend upon abstractions, not concretions.
 * Higher level components must not depend on lower level components in their implementation. Instead, lower-level
 * components must implement an interface defined by the higher level component thus inverting the dependency.
 * Interfaces may be defined in an independent library to reduce dependency on a higher level component.
 */

private object DependencyInversionViolated {

    // Higher level UserRepository depends on lower level FirebaseAuth in its implementation.
    // We are forced to use FirebaseAuth when we use UserRepository.
    class UserRepository(
        private val auth: FirebaseAuth,
        private val fileLogger: FileLogger
    ) {

        fun signInUser(name: String, password: String) {
            try {
                auth.signIn(name, password)
            } catch (e: Exception) {
                fileLogger.logError(e.message.toString())
            }
        }
    }

    class FirebaseAuth {

        fun signIn(name: String, password: String) {
            // access Firebase
        }
    }

    class FileLogger {

        fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }
}

private object DependencyInversionFixed {

    // UserRepository doesn't depend on lower level FirebaseAuth,
    // we can use any Authenticator implementation
    class UserRepository(
        private val auth: Authenticator,
        private val fileLogger: FileLogger
    ) {

        fun signInUser(name: String, password: String) {
            try {
                auth.signIn(name, password)
            } catch (e: Exception) {
                fileLogger.logError(e.message.toString())
            }
        }
    }

    interface Authenticator {

        fun signIn(name: String, password: String)
    }

    // dependency is inversed: FirebaseAuth depends on Authenticator defined by UserRepository
    class FirebaseAuth : Authenticator {

        override fun signIn(name: String, password: String) {
            // access Firebase
        }
    }

    class CustomApiAuth: Authenticator {

        override fun signIn(name: String, password: String) {
            // access custom API
        }
    }

    class FileLogger {

        fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }
}
