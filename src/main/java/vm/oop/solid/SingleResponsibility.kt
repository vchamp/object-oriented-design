package vm.oop.solid

import java.io.File

/*
 * Single responsibility principle:
 * Class should have only one responsibility and a reason to change.
 * Makes the class more robust. It’s less possible to break a code not related to the change reason.
 */

private object SingleResponsibilityViolated {

    // Violation: UserRepository has more than one reason to change: how to sign in and how to log an error

    class UserRepository(
        private val auth: (name: String, password: String) -> Unit
    ) {

        fun signInUser(name: String, password: String) {
            try {
                auth(name, password)
            } catch (e: Exception) {
                val file = File("errors.txt")
                file.appendText(e.message.toString())
            }
        }
    }
}

private object SingleResponsibilityFixed {

    // Single responsibility - signing in users
    class UserRepository(
        private val auth: (name: String, password: String) -> Unit,
        private val fileLogger: FileLogger
    ) {

        fun signInUser(name: String, password: String) {
            try {
                auth(name, password)
            } catch (e: Exception) {
                fileLogger.logError(e.message.toString())
            }
        }
    }

    // Single responsibility - logging errors
    class FileLogger {

        fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }
}
