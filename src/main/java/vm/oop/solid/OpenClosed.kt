package vm.oop.solid

import java.io.File

/*
 * Open-closed principle:
 * Classes, modules, functions, etc. should be open for extension, but closed for modification.
 * An entity should allow its behavior to be extended without modifying its source code.
 */

private object OpenClosedViolated {

    // Violation: we may want to change where the errors are saved, e.g. the file name, this will make all users of this
    // class to use the new file name

    class FileLogger {

        fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }
}

private object OpenClosedFixed {

    // File logger is now open for extension
    open class FileLogger {

        open fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }

    // We can use this class where a new file name is needed,
    // all other users will continue using FileLogger with unchanged file name
    class CustomFileLogger : FileLogger() {

        override fun logError(error: String) {
            val file = File("custom_error_file.txt")
            file.appendText(error)
        }
    }
}
