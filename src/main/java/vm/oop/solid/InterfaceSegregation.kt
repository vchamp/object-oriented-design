package vm.oop.solid

import java.io.File

/*
 * Interface segregation principle:
 * No client should be forced to depend on methods it does not use.
 * Split to smaller interfaces, so that a client only knows about the methods it needs.
 */

private object InterfaceSegregationViolated {

    interface FileLogger {

        fun printLogs()

        fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }

    class CustomFileLogger : FileLogger {

        override fun printLogs() {
            // printing is not supported for custom file, so we left this function empty,
            // this may result in unexpected behavior for the class users
        }

        override fun logError(error: String) {
            val file = File("custom_error_file.txt")
            file.appendText(error)
        }
    }
}

private object InterfaceSegregationFixed {

    // split to smaller interfaces

    // Pass it to clients that only need to know about logging.
    interface FileLogger {

        fun logError(error: String) {
            val file = File("errors.txt")
            file.appendText(error)
        }
    }

    // Pass it to clients that only need to know about printing.
    interface Printable {

        fun printLogs()
    }

    // CustomFileLogger is not forced to use the method it doesn't need
    class CustomFileLogger : FileLogger {

        override fun logError(error: String) {
            val file = File("custom_error_file.txt")
            file.appendText(error)
        }
    }

    class PrintableCustomFileLogger: FileLogger, Printable {

        override fun printLogs() {
            println(File("custom_error_file.txt").readText())
        }

        override fun logError(error: String) {
            val file = File("custom_error_file.txt")
            file.appendText(error)
        }
    }
}
