package vm.oop.principles

/*
 * Composition over inheritance principle:
 * Classes should achieve polymorphic behavior and code reuse by their composition (by containing instances of other
 * classes that implement the desired functionality) rather than inheritance from a base or parent class.
 * It is easier to define what features the class has than what it is.
 */

private object CompositionOverInheritance {

    // Failed inheritance

    abstract class Transport {

        abstract fun move()
    }

    abstract class ManPowered : Transport() {

        abstract fun setCrew(number: Int)
    }

    abstract class EnginePowered : Transport() {

        abstract fun setEngine(type: String)
    }

    abstract class WaterFairer : Transport() {

        abstract val volume: Int
        abstract fun swim()
    }

    abstract class GroundRoller : Transport() {

        abstract val wheelNumber: Int
        abstract fun roll()
    }

    //class Boat() : ManPowered(), WaterFairer() - can't do with inheritance
    //class Car() : EnginePowered(), GroundRoller() - can't do with inheritance

    // Composition

    class ManPower {
        private var crew: Int = 0
        fun setCrew(n: Int) {
            crew = n
        }
    }

    class EnginePower {
        private var engineType: String = ""
        fun setEngine(type: String) {
            engineType = type
        }
    }

    class WaterFairing {
        var volume: Int = 0
        fun swim() {}
    }

    class GroundRolling {
        var wheelNumber: Int = 0
        fun roll() {}
    }

    class Boat(
        private val manPower: ManPower,
        private val waterFairing: WaterFairing
    ) : Transport() {
        override fun move() {
            manPower.setCrew(4)
            waterFairing.swim()
        }
    }

    class Car(
        private val enginePower: EnginePower,
        private val groundRolling: GroundRolling
    ) : Transport() {
        override fun move() {
            enginePower.setEngine("electric")
            groundRolling.roll()
        }
    }

    // ---------------------------
    // Composition
    // ---------------------------

    class ClosedPerson(val name: String, val dateOfBirth: String) {

        fun describe(): String {
            return "name: $name, born: $dateOfBirth"
        }
    }

    interface Participant {

        fun describe(): String
    }

    class EmployeeParticipant(
        // person is hidden
        private val person: ClosedPerson
    ) : Participant {

        override fun describe(): String {
            return "Employee - ${person.describe()}"
        }
    }

    class StudentParticipant(
        // person is hidden
        private val person: ClosedPerson
    ) : Participant {

        override fun describe(): String {
            return "Student - ${person.describe()}"
        }
    }

    fun compositionUseCase() {
        val participants = listOf(
            EmployeeParticipant(ClosedPerson("Bob", "12-12-1980")),
            StudentParticipant(ClosedPerson("Alice", "30-08-1990"))
        )
        for (participant in participants) {
            // polymorphic behavior achieved by composition
            println(participant.describe())
        }
    }

    // ---------------------------
    // Inheritance
    // ---------------------------

    open class OpenPerson(val name: String, val dateOfBirth: String) {

        open fun describe(): String {
            return "name: $name, born: $dateOfBirth"
        }
    }

    class Employee(name: String, dateOfBirth: String) : OpenPerson(name, dateOfBirth) {

        override fun describe(): String {
            return "Employee - ${super.describe()}"
        }
    }

    class Student(name: String, dateOfBirth: String) : OpenPerson(name, dateOfBirth) {

        override fun describe(): String {
            return "Student - ${super.describe()}"
        }
    }

    fun inheritanceUseCase() {
        val persons = listOf(
            Employee("Bob", "12-12-1980"),
            Student("Alice", "30-08-1990")
        )
        for (person in persons) {
            // polymorphic behavior achieved by inheritance
            println(person.describe())
        }
    }
}
