package vm.oop.concepts

/*
 * Inheritance principle:
 * The mechanism by which one class is allowed to inherit the features (fields and methods) of another class.
 * Inheritance is also known as “is-a” relationship.
 */

private object Inheritance {

    // Implementation inheritance

    open class Person(private val name: String, private val dateOfBirth: String) {

        fun getName(): String {
            return name
        }

        open fun describe(): String {
            return "name: $name, born: $dateOfBirth"
        }
    }

    // Employee "is a" Person
    class Employee(name: String, dateOfBirth: String) : Person(name, dateOfBirth) {

        override fun describe(): String {
            return "Employee - ${super.describe()}"
        }
    }

    // Student "is a" Person
    class Student(name: String, dateOfBirth: String) : Person(name, dateOfBirth) {

        override fun describe(): String {
            return "Student - ${super.describe()}"
        }
    }

    fun useCase() {
        val employee = Employee("Alice", "10-10-2002")
        // inherited feature
        println(employee.getName())
        // overridden feature
        println(employee.describe())
    }
}
