package vm.oop.concepts

/*
 * Polymorphism principle:
 * Ability of a calling code to be independent of which class in the supported hierarchy it is operating on – the parent
 * class or one of its descendants.
 * Ability of a functionality to be provided in more than one form.
 * Can be compile-time (overloading), static-time (overriding) and parametric (generics) polymorphism.
 */

private object Polymorphism {

    // Compile-time polymorphism

    class Appender {

        fun add(p1: Int, p2: Int): Int {
            return p1 + p2
        }

        fun add(p1: String, p2: String): String {
            return p1 + p2
        }
    }

    fun appenderUseCase() {
        val appender = Appender()
        println(appender.add(1, 2))
        println(appender.add("1", "2"))
    }

    // Static-time polymorphism

    abstract class OutputDevice {

        abstract fun output(s: String)
    }

    class Display : OutputDevice() {

        override fun output(s: String) {
            // display string
        }
    }

    class Printer : OutputDevice() {

        override fun output(s: String) {
            // print string
        }
    }

    // Parametric polymorphism

    class ValueHolder<T> {

        private var value: T? = null

        fun setValue(v: T) {
            value = v
        }
    }

    fun valueHolderUseCase() {
        val stringHolder = ValueHolder<String>()
        stringHolder.setValue("23")
        val intHolder = ValueHolder<Int>()
        intHolder.setValue(23)
    }
}
