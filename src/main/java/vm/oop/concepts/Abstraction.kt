package vm.oop.concepts

/*
 * Abstraction principle:
 * Hide the internal implementation details.
 * A class does not allow calling code to access internal object data and permits access through methods only.
 */

private object Abstraction {

    // assume outside class
    class Driver {

        private val car = Car()

        fun drive() {
            // Driver doesn't know how car accelerates
            car.accelerate()
            // Driver doesn't know how car brakes
            car.brake()
        }
    }

    class Car {

        private val engine = Engine()
        private val brakingSystem = BrakingSystem()

        // Driver doesn't know how Car accelerates
        fun accelerate() {
            // Car doesn't know how Engine adds power
            engine.addPower()
            // engine.fuelFlow++ - can't do
        }

        // Driver doesn't know how Car brakes
        fun brake() {
            // Car doesn't know how BrakingSystem brakes
            brakingSystem.brake()
            // brakingSystem.fluidPressure++ - can't do
        }
    }

    // internal class is hidden for a Driver outside module
    internal class Engine() {

        // this is hidden for Car
        private var fuelFlow = 0

        fun addPower() {
            fuelFlow++
        }
    }

    // internal class is hidden for a Driver outside module
    internal class BrakingSystem() {

        // this is hidden for Car
        private var fluidPressure = 0

        fun brake() {
            fluidPressure++
        }
    }
}
