package vm.oop.concepts

/*
 * Encapsulation principle:
 * Wrapping up of data under a single unit.
 * Binds together code and the data it manipulates.
 * Prevents the data from being accessed by the code outside.
 */

private object Encapsulation {

    class Geometry {

        private val rectangle = Rectangle(left = 10, top = 10)

        fun change() {
            rectangle.setWidth(20)
            rectangle.setHeight(40)
            rectangle.move(5, 0)
            // rectangle.left += 5 - can't do, would change width without changing right
        }
    }

    class Rectangle(
        // private properties encapsulated in a Rectangle
        private var left: Int,
        private var top: Int
    ) {

        // private properties encapsulated in a Rectangle
        private var right: Int = left
        private var bottom: Int = top

        // data is manipulated with code
        fun setWidth(width: Int) {
            right = left + width
        }

        fun getWidth(): Int {
            return right - left
        }

        // data is manipulated with code
        fun setHeight(height: Int) {
            bottom = top + height
        }

        fun getHeight(): Int {
            return bottom - top
        }

        fun getPosition(): Pair<Int, Int> {
            return left to top
        }

        // data is manipulated with code
        fun move(dx: Int, dy: Int) {
            left += dx
            right += dx
            top += dy
            bottom += dy
        }
    }
}
